proc setupCmd {} {
  global array_cmd
  array set array_cmd [list {quit} {quit $sock} \
                         {menu} {menu $sock} \
                         {stime} {stime $sock} \
                         {ltime} {ltime $sock} \
                         {num_c} {num_c $sock} \
                         {num_r} {num_r $sock} \
                         {echo} {echo $sock $str1} ]
}



# Command section
proc quit {aSock} {
  global connClients
  set idx [lsearch $connClients $aSock]
  set connClients [lreplace $connClients $idx $idx]
  set msg "Bye bye, dude"
  puts $aSock $msg
  close $aSock
}

proc menu {aSock} {
  puts $aSock "List of the command accepted:"
  puts $aSock " menu - return this help screen"
  puts $aSock " stime - current time on the server"
  puts $aSock " ltime - current date and time on the server" 
  puts $aSock " num_c - number of currently connected client" 
  puts $aSock " num_r - total number of request received"
  puts $aSock " echo <string> -  echo the string " 
}

proc stime {aSock} {
  set sTime [clock seconds]
  puts $aSock "Server time: [clock format $sTime -format %H:%M:%S]"
}

proc ltime {aSock} {
  set sTime [clock seconds]
  puts $aSock "Server date> [clock format $sTime]"
}

proc num_c {aSock} {
  global connClients 
  puts $aSock "number of connected client: [llength $connClient]"
}

proc num_r {aSock} {
  global totReq
  puts $aSock "number of request received: $totReq"
}

proc echo {aSock str} {
  puts $aSock $str
}


proc every {ms body} {
    global every
    if {$ms == "cancel"} {after cancel $every($body); unset every($body); return}
    set every($body) [info level 0]
    eval $body
    after $ms [info level 0]
}


proc myping {} {
  global connClients
  puts "Clients connected and to ping: [llength $connClients]"
  
  foreach ss $connClients {
    puts $ss "ping..."
  }
}


# end of Command section

proc srvCommand {sock} {
  global array_cmd
  global totReq

  incr totReq
  set cmd [gets $sock]
  set cmdList [split $cmd " "]
  puts "$sock - Command received $cmd"
  set ll [llength $cmdList]
  # make it resilient
  catch { 
    #puts "cmdList $cmdList"
    if {$ll > 0} {
      set userCmd [lindex $cmdList 0]
      if {$ll > 1} {
        set str1 [lindex $cmdList 1]
      }
      eval $array_cmd($userCmd)
      puts "Command executed $cmd"
    } 
  }
}


proc acceptConn {sock addr port} {
  global array_cmd
  global connClients

  fconfigure $sock -buffering line
  # remember the client connected
  lappend connClients $sock
  puts "$addr:$port connected"
  fileevent $sock readable [list srvCommand $sock]
}

setupCmd
set totReq 0
set connClients [list] 
#puts [array names array_cmd]
if {$argc > 0} {
  set evr [lindex $argv 0]
  # at least 1 second to wait for the ping 
  if {$evr < 1000} { 
    set evr 5000
  }
} else {
  set evr 5000
}
every $evr [list myping]
socket -server acceptConn 33774
vwait forever
