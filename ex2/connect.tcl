load libOT_InfTcl.so

set DB [inf_open_conn willhill_dev@db01_1150]

set select_sql {
  select
    t.tabname
  from
    systables t
  where
    t.tabid >= 100 and
    t.tabtype = 'T'
}

set sqlquery [inf_prep_sql $DB $select_sql]

set res_set [inf_exec_stmt $sqlquery]

inf_close_stmt $sqlquery

for {set i 0} {$i < [db_get_nrows $res_set]} {incr i} {
  puts "$i table name: [db_get_col $res_set tabname]"
}

puts "tha's all folks!!!"

