insert into tspaluellwins (acct_id, cr_date, winnings)
select acct_id, extend(cr_date, year to day), sum(winnings)
from tbet t
where t.winnings > 0
group by 1, 2
