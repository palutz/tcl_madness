load libOT_InfTcl.so


proc connectDB {dbname} {
  return [inf_open_conn $dbname]
}


proc removeSummaryNoDate {adb} {
  set qry {
    delete from tspaluellwins
  }
  set clearStmt [inf_prep_sql $adb $qry]
  inf_exec_stmt $clearStmt
  inf_close_stmt $clearStmt
}


proc removeSummaryWDate {adb aday} {
  set qry { 
    delete 
    from 
      tspaluellwins 
    where 
      extend(cr_date, year to day) = ?
  }
  set clearStmt [inf_prep_sql $adb $qry]
  inf_exec_stmt $clearStmt $aday
  inf_close_stmt $clearStmt
}


proc insertQueryWDate {} {
  return {
    insert into tspaluellwins (cr_date, acct_id, winnings)
    select 
      extend(b.cr_date, year to day),
      b.acct_id,
      sum(b.winnings)
    from
      tbet b
    where
      extend(b.cr_date, year to day) = ? and 
      b.winnings > 0 
    group by 
      1,
      2
    }
}


proc insertQueryNoDate {} {
  return {
    insert into tspaluellwins (cr_date, acct_id, winnings)
    select 
      extend(b.cr_date, year to day),
      b.acct_id,
      sum(b.winnings)
    from
      tbet b
    where
      b.winnings > 0 
    group by 
      1,
      2 
    }
}


proc insertSummaryWDate {adb aday} {
  set qry [insertQueryWDate]
  set insQuery [inf_prep_sql $adb $qry]
  inf_exec_stmt $insQuery $aday
  inf_close_stmt $insQuery
}


proc insertSummaryNoDate {adb} {
  set qry [insertQueryNoDate]
  set insQuery [inf_prep_sql $adb $qry]
  inf_exec_stmt $insQuery
  inf_close_stmt $insQuery
}


proc summaryWithDate {adb aday} {
  puts "removing old values..."
  removeSummaryWDate $adb $aday
  puts "inserting new summary..."
  insertSummaryWDate $adb $aday
  puts "That's all, folks!!!"
}


proc summaryWithNoDate {adb} {
  puts "removing old values..."
  removeSummaryNoDate $adb
  puts "inserting new summary..."
  insertSummaryNoDate $adb
  puts "That's all, folks!!!"
}


proc insertWinning {adb {aday ""}} {
  set lStr [string length $aday]
  if { $lStr > 0 } {
    summaryWithDate $adb $aday
  } else {
    summaryWithNoDate $adb
  }
}

# ...
set DB [connectDB willhill_dev]

if {$argc > 0} {
  set adate [lindex $argv 0]
  set isCorrect [regexp {\d{4}[-]\d{1,2}[-]\d{1,2}} $adate] 
  if { $isCorrect == 1} {
    puts "Date to be processed:$adate"
    insertWinning $DB $adate
  }  else {
    puts "Date format not valid!"
  }
} else {
  puts "No date inserted"
  insertWinning $DB
}
#puts [insertWinning $DB {2011-10-09}]
#puts [insertWinning $DB]
