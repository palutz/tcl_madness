create table tspaluellwins (
  acct_id 	INT,
  cr_date	DATETIME YEAR TO DAY,
  winnings	DECIMAL(12,2) CHECK (winnings >= 0),
  PRIMARY KEY (cr_date, acct_id)
);
